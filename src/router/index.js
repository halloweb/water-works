import Vue from 'vue'
import VueRouter from 'vue-router'
import HistoryReappear from '@/views/history-reappear'
import AccurateBQ from '@/views/accurate-BQ'
import AddMedicine from '@/views/add-medicine'
import ProduceParam from '@/views/produce-param'
import AlarmRecord from '@/views/alarm-record'
import RunRecord from '@/views/run-record'
import CraftMonitor from '@/views/craft-monitor'
import FactoryOverview from '@/views/factory-overview'
import DataDriven from '@/views/data-driven'
import Evaluate from '@/views/evaluate'
import OperationManage from '@/views/operation-manage'
Vue.use(VueRouter)

const routes = [
  {
    path: '/historyReappear',
    name: 'historyReappear',
    component: HistoryReappear
  },
  {
    path: '/accurateBQ',
    name: 'accurateBQ',
    component: AccurateBQ
  },
  {
    path: '/addMedicine',
    name: 'addMedicine',
    component: AddMedicine
  },
  {
    path: '/produceParam',
    name: 'produceParam',
    component: ProduceParam
  },
  {
    path: '/alarmRecord',
    name: 'alarmRecord',
    component: AlarmRecord
  },
  {
    path: '/runRecord',
    name: 'runRecord',
    component: RunRecord
  },
  {
    path: '/craftMonitor',
    name: 'craftMonitor',
    component: CraftMonitor
  },
  {
    path: '/factoryOverview',
    name: 'factoryOverview',
    component: FactoryOverview
  },
  {
    path: '/dataDriven',
    name: 'dataDriven',
    component: DataDriven
  },
  {
    path: '/evaluate',
    name: 'evaluate',
    component: Evaluate
  },
  {
    path: '/operationManage',
    name: 'operationManage',
    component: OperationManage
  }
]

const router = new VueRouter({
  routes
})

export default router
